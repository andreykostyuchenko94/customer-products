package com.appsmart.customerproducts.model.request

import java.math.BigDecimal
import kotlin.reflect.full.memberProperties

abstract class AbstractRequest

inline fun <reified T : AbstractRequest> AbstractRequest.toMap(): MutableMap<String, Any?> {
    val map = emptyMap<String, Any?>().toMutableMap()
    T::class.memberProperties.forEach { prop ->
        val value = prop.get(this as T)
        if (value != null) {
            map[prop.name] = value
        }
    }
    return map
}

data class ProductRequest(
    val title: String,
    val price: BigDecimal,
    val description: String? = null,
) : AbstractRequest()


data class CustomerRequest(
    val title: String
) : AbstractRequest()