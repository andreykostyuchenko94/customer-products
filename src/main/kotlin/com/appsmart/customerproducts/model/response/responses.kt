package com.appsmart.customerproducts.model.response

import com.appsmart.customerproducts.model.Customer
import java.math.BigDecimal
import java.time.Instant
import java.util.*


data class ProductResponse(
    val id: UUID,
    val customer: Customer,
    val title: String,
    val description: String? = null,
    val price: BigDecimal,
    val isDeleted: Boolean,
    val createdAt: Instant,
    val modifiedAt: Instant? = null
)
