package com.appsmart.customerproducts.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.CompoundIndex
import org.springframework.data.mongodb.core.index.CompoundIndexes
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.Instant
import java.util.*

@Document(collection = "products")
@CompoundIndexes(
    value = [
        CompoundIndex(name = "customerId_1_title_1", def = "{'customerId' : 1, 'title': 1}", unique = true),
        CompoundIndex(name = "customerId_1_isDeleted_1", def = "{'customerId' : 1, 'isDeleted': 1}")
    ]
)
data class Product(
    @Id
    val id: UUID = UUID.randomUUID(),

    val customerId: UUID,

    val title: String,

    val description: String? = null,

    val price: BigDecimal,

    val isDeleted: Boolean = false,

    val createdAt: Instant = Instant.now(),

    val modifiedAt: Instant? = null
)
