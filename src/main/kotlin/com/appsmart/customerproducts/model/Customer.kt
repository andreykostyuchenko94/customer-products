package com.appsmart.customerproducts.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant
import java.util.*

@Document(collection = "customers")
data class Customer(
    @Id
    val id: UUID = UUID.randomUUID(),

    @Indexed(name = "title_1", unique = true)
    val title: String,

    @Indexed(name = "isDeleted_1")
    val isDeleted: Boolean = false,

    val createdAt: Instant = Instant.now(),

    val modifiedAt: Instant? = null
)