package com.appsmart.customerproducts.dao

import kotlinx.coroutines.reactor.awaitSingle
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@Repository
class SmartReactiveMongoTemplate(val mongoTemplate: ReactiveMongoTemplate)

inline fun <reified T : Any> SmartReactiveMongoTemplate.updateOne(
    id: Any,
    props: Map<String, Any?>
) {
    val query = Query(Criteria("_id").`is`(id))
    val update = Update()
    props.forEach { (k, v) -> update.set(k, v) }

    mongoTemplate.updateFirst(query, update, T::class.java).subscribe()
}

inline fun <reified T : Any, reified O> SmartReactiveMongoTemplate.aggregate(filter: Map<String, Any>? = null): Flux<O> {
    val match = Aggregation.match(criteriaOf(filter))
    val lookup = Aggregation.lookup("customers", "customerId", "_id", "customer")
    val unwind = Aggregation.unwind("customer")

    val aggregation = Aggregation.newAggregation(T::class.java, match, lookup, unwind)
    return mongoTemplate.aggregate(aggregation, O::class.java)
}

inline fun <reified T : Any> SmartReactiveMongoTemplate.save(entity: T): Mono<T> = mongoTemplate.save(entity)

inline fun <reified T : Any> SmartReactiveMongoTemplate.findById(id: Any): Mono<T> =
    mongoTemplate.findById(id, T::class.java)

suspend inline fun <reified T : Any> SmartReactiveMongoTemplate.count(filter: Map<String, Any>? = null): Long {
    return mongoTemplate.count(Query(criteriaOf(filter)), T::class.java).awaitSingle()
}

inline fun <reified T : Any> SmartReactiveMongoTemplate.findAll(
    page: Int, pageSize: Int,
    filter: Map<String, Any>? = null
): Flux<T> {
    return mongoTemplate.find(
        Query(criteriaOf(filter)).with(Pageable.ofSize(pageSize).withPage(page)),
        T::class.java
    )
}

fun criteriaOf(filter: Map<String, Any>?): Criteria {
    val criteria = Criteria()
    filter?.forEach { (k, v) -> criteria.and(k).`is`(v) }

    return criteria
}
