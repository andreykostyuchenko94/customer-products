package com.appsmart.customerproducts.config

import org.slf4j.LoggerFactory
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler
import org.springframework.context.annotation.Configuration
import org.springframework.dao.DuplicateKeyException
import org.springframework.http.HttpStatus
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

@Configuration
class SmartExceptionHandler : ErrorWebExceptionHandler {
    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun handle(exchange: ServerWebExchange, ex: Throwable): Mono<Void> {
        logger.info(ex.stackTraceToString())

        when (ex) {
            is DuplicateKeyException -> exchange.response.statusCode = HttpStatus.UNPROCESSABLE_ENTITY
            is NoSuchElementException -> exchange.response.statusCode = HttpStatus.NOT_FOUND
            is Exception -> exchange.response.statusCode = HttpStatus.INTERNAL_SERVER_ERROR
        }

        return Mono.empty()
    }
}