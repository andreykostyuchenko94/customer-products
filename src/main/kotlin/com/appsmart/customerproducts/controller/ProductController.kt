package com.appsmart.customerproducts.controller

import com.appsmart.customerproducts.model.Product
import com.appsmart.customerproducts.model.request.ProductRequest
import com.appsmart.customerproducts.model.response.ProductResponse
import com.appsmart.customerproducts.service.ProductService
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/v1")
class ProductController(
    private val productService: ProductService
) {
    @GetMapping("/customers/{customerId}/products")
    suspend fun list(
        @PathVariable customerId: UUID,
        @RequestParam page: Int,
        @RequestParam pageSize: Int
    ): Collection<ProductResponse>? {
        return productService.findByCustomer(customerId, page, pageSize)
    }

    @PostMapping("/customers/{customerId}/products")
    suspend fun create(
        @PathVariable customerId: UUID,
        @RequestBody request: ProductRequest
    ): Product {
        return productService.create(customerId, request)
    }

    @DeleteMapping("/products/{productId}")
    suspend fun delete(@PathVariable productId: UUID) {
        productService.delete(productId)
    }

    @GetMapping("/products/{productId}")
    suspend fun get(@PathVariable productId: UUID): ProductResponse {
        return productService.findById(productId)
    }

    @PutMapping("/products/{productId}")
    suspend fun update(
        @PathVariable productId: UUID,
        @RequestBody request: ProductRequest
    ) {
        productService.update(productId, request)
    }
}