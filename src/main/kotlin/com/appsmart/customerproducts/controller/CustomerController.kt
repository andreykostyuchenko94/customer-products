package com.appsmart.customerproducts.controller

import com.appsmart.customerproducts.model.Customer
import com.appsmart.customerproducts.model.request.CustomerRequest
import com.appsmart.customerproducts.service.CustomerService
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/v1/customers")
class CustomerController(
    private val customerService: CustomerService
) {
    @GetMapping
    suspend fun list(
        @RequestParam page: Int,
        @RequestParam pageSize: Int
    ): List<Customer> {
        return customerService.findAll(page, pageSize)
    }

    @PostMapping
    suspend fun create(@RequestBody request: CustomerRequest): Customer {
        return customerService.create(request)
    }

    @DeleteMapping("/{customerId}")
    suspend fun delete(@PathVariable customerId: UUID) {
        customerService.delete(customerId)
    }

    @GetMapping("/{customerId}")
    suspend fun get(@PathVariable customerId: UUID): Customer {
        return customerService.findById(customerId)
    }

    @PutMapping("/{customerId}")
    suspend fun update(
        @PathVariable customerId: UUID,
        @RequestBody request: CustomerRequest
    ) {
        customerService.update(customerId, request)
    }
}