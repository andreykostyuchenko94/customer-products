package com.appsmart.customerproducts.service

import com.appsmart.customerproducts.model.Product
import com.appsmart.customerproducts.model.request.ProductRequest
import com.appsmart.customerproducts.model.response.ProductResponse
import java.util.*

interface ProductService {

    suspend fun create(customerId: UUID, request: ProductRequest): Product

    suspend fun findByCustomer(customerId: UUID, page: Int, pageSize: Int): List<ProductResponse>?

    suspend fun delete(productId: UUID)

    suspend fun findById(productId: UUID): ProductResponse

    suspend fun update(productId: UUID, request: ProductRequest)
}