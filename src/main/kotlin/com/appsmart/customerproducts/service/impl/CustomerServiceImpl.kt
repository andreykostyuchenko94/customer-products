package com.appsmart.customerproducts.service.impl

import com.appsmart.customerproducts.dao.*
import com.appsmart.customerproducts.model.Customer
import com.appsmart.customerproducts.model.request.CustomerRequest
import com.appsmart.customerproducts.model.request.toMap
import com.appsmart.customerproducts.service.CustomerService
import kotlinx.coroutines.reactor.awaitSingle
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.*

@Service
class CustomerServiceImpl(
    private val smartReactiveMongoTemplate: SmartReactiveMongoTemplate
) : CustomerService {

    companion object {
        private val deletedFilter = mapOf(Customer::isDeleted.name to false)
    }

    override suspend fun create(request: CustomerRequest): Customer {
        val customer = Customer(title = request.title)
        return smartReactiveMongoTemplate.save(customer).awaitSingle()
    }

    override suspend fun delete(customerId: UUID) {
        val props = mapOf(
            Customer::isDeleted.name to true,
            Customer::modifiedAt.name to Instant.now()
        )
        smartReactiveMongoTemplate.updateOne<Customer>(customerId, props)
    }

    override suspend fun findById(customerId: UUID): Customer {
        return smartReactiveMongoTemplate
            .findById<Customer>(customerId)
            .awaitSingle()
    }

    override suspend fun findAll(page: Int, pageSize: Int): List<Customer> {
        return smartReactiveMongoTemplate
            .findAll<Customer>(page, pageSize, deletedFilter)
            .collectList()
            .awaitSingle()
    }

    override suspend fun update(customerId: UUID, request: CustomerRequest) {
        val props = request.toMap<CustomerRequest>().also {
            it[Customer::modifiedAt.name] = Instant.now()
        }
        smartReactiveMongoTemplate.updateOne<Customer>(customerId, props)
    }
}


