package com.appsmart.customerproducts.service.impl

import com.appsmart.customerproducts.dao.*
import com.appsmart.customerproducts.model.Customer
import com.appsmart.customerproducts.model.Product
import com.appsmart.customerproducts.model.request.ProductRequest
import com.appsmart.customerproducts.model.request.toMap
import com.appsmart.customerproducts.model.response.ProductResponse
import com.appsmart.customerproducts.service.ProductService
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactor.awaitSingle
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import java.time.Instant
import java.util.*

@Service
class ProductServiceImpl(
    private val smartReactiveMongoTemplate: SmartReactiveMongoTemplate
) : ProductService {
    override suspend fun create(customerId: UUID, request: ProductRequest): Product {
        val filter = mapOf(
            Customer::id.name to customerId,
            Customer::isDeleted.name to false
        )
        if (smartReactiveMongoTemplate.count<Customer>(filter) < 1) {
            throw HttpClientErrorException(HttpStatus.NOT_FOUND)
        }
        val product = Product(
            customerId = customerId,
            title = request.title,
            price = request.price,
            description = request.description
        )
        return smartReactiveMongoTemplate.save(product).awaitSingle()
    }

    override suspend fun findByCustomer(
        customerId: UUID,
        page: Int, pageSize: Int
    ): List<ProductResponse>? {
        val filter = mapOf(
            Product::customerId.name to customerId,
            Product::isDeleted.name to false
        )
        return smartReactiveMongoTemplate
            .aggregate<Product, ProductResponse>(filter)
            .collectList()
            .awaitSingle()
    }

    override suspend fun delete(productId: UUID) {
        val props = mapOf(
            Customer::isDeleted.name to true,
            Customer::modifiedAt.name to Instant.now()
        )
        smartReactiveMongoTemplate.updateOne<Product>(productId, props)
    }

    override suspend fun findById(productId: UUID): ProductResponse {
        val filter = mapOf(Product::id.name to productId)
        return smartReactiveMongoTemplate
            .aggregate<Product, ProductResponse>(filter)
            .awaitFirst()
    }

    override suspend fun update(productId: UUID, request: ProductRequest) {
        val props = request.toMap<ProductRequest>().also {
            it[Customer::modifiedAt.name] = Instant.now()
        }
        smartReactiveMongoTemplate.updateOne<Product>(productId, props)
    }
}
