package com.appsmart.customerproducts.service

import com.appsmart.customerproducts.model.Customer
import com.appsmart.customerproducts.model.request.CustomerRequest
import java.util.*

interface CustomerService {

    suspend fun create(request: CustomerRequest): Customer

    suspend fun delete(customerId: UUID)

    suspend fun findById(customerId: UUID): Customer

    suspend fun findAll(page: Int, pageSize: Int): List<Customer>

    suspend fun update(customerId: UUID, request: CustomerRequest)
}