package com.appsmart.customerproducts

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CustomerProductsApplication

fun main(args: Array<String>) {
    runApplication<CustomerProductsApplication>(*args)
}
